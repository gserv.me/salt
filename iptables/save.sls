include:
  - init/iptables

iptables_save:
  cmd.run:
    - name: "iptables-save -f /etc/iptables/iptables.rules"
    - hide_output: True
