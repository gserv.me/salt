us_locale:
  locale.present:
    - name: en_US.UTF-8

gb_locale:
  locale.present:
    - name: en_GB.UTF-8

init_locales:
  locale.system:
    - name: en_US.UTF-8
    - require:
      - locale: us_locale
      - locale: gb_locale

