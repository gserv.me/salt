init_nginx:
  pkg.installed:
    - pkgs:
        - nginx

  service.running:
    - name: nginx

    - enable: True
    - reload: True

    - watch:
        - file: init_nginx_files

        - acme: archivesmc.com

        - acme: feeds.gareth-coles.dev
        - acme: fedi.gareth-coles.dev
        - acme: immich.gareth-coles.dev

        - acme: repo.glowstone.net

        - acme: jm.gserv.me
        - acme: portainer.gserv.me
        - acme: sync.gserv.me

        - acme: kenmarecraftcentre.com

        - acme: kotlindiscord.com
        - acme: kordex.kotlindiscord.com
        - acme: wiki.kotlindiscord.com

        - acme: data.kordex.dev
        - acme: dev.kordex.dev
        - acme: repo.kordex.dev
        - acme: stats.kordex.dev


init_nginx_files:
  file.recurse:
    - name: /etc/nginx/sites-enabled
    - source: salt://etc/nginx/sites-enabled/

# SSL certs

archivesmc.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: archivesmc.com
    - aliases:
        - www.archivesmc.com

fedi.gareth-coles.dev:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: fedi.gareth-coles.dev

feeds.gareth-coles.dev:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: feeds.gareth-coles.dev

immich.gareth-coles.dev:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: immich.gareth-coles.dev


repo.glowstone.net:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: repo.glowstone.net


jm.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: jm.gserv.me

portainer.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: portainer.gserv.me

sync.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: sync.gserv.me


kenmarecraftcentre.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: kenmarecraftcentre.com
    - aliases:
        - www.kenmarecraftcentre.com


kotlindiscord.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: kotlindiscord.com

kordex.kotlindiscord.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: kordex.kotlindiscord.com

wiki.kotlindiscord.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: wiki.kotlindiscord.com


data.kordex.dev:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: data.kordex.dev


dev.kordex.dev:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: dev.kordex.dev

repo.kordex.dev:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: repo.kordex.dev

stats.kordex.dev:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: stats.kordex.dev
