init_sudo:
  pkg.installed:
    - pkgs:
      - sudo

  file.managed:
    - name: /etc/sudoers
    - source: salt://etc/sudoers
