init_iptables:
  pkg.installed:
    - pkgs:
      - iptables

  service.running:
    - name: iptables
    - enable: true

# AMP iptables rules

iptables_amp_tcp_40000:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 40000
    - jump: ACCEPT

iptables_amp_udp_40000:
  iptables.append:
    - chain: INPUT
    - protocol: udp
    - dport: 40000
    - jump: ACCEPT

iptables_amp_tcp_40001:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 40001
    - jump: ACCEPT

iptables_amp_udp_40001:
  iptables.append:
    - chain: INPUT
    - protocol: udp
    - dport: 40001
    - jump: ACCEPT


iptables_amp_tcp_40002:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 40002
    - jump: ACCEPT

iptables_amp_udp_40002:
  iptables.append:
    - chain: INPUT
    - protocol: udp
    - dport: 40002
    - jump: ACCEPT

iptables_amp_tcp_40003:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 40003
    - jump: ACCEPT

iptables_amp_udp_40003:
  iptables.append:
    - chain: INPUT
    - protocol: udp
    - dport: 40003
    - jump: ACCEPT
