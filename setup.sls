setup:
#  pkg.installed:
#    - pkgs:
#      - base-devel
#      - git
#      - haveged
#      - htop
#      - mosh
#      - nano
#      - net-tools
#      - p7zip
#      - python
#      - python-cherrypy
#      - python-dateutil
#      - python-docker
#      - python-pip
#      - python2
#      - python2-cherrypy
#      - python2-dateutil
#      - python2-pip
#      - screen
#      - sudo

  user.present:
    - name: gdude
    - home: /home/gdude

  timezone.system:
    - name: Europe/Dublin

#update:
#  pkg.uptodate:
#    - name: "Update"
#    - refresh: True
