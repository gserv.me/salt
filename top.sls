base:
  "*":
    - setup

    - init/keys
    - init/locales

    - init/docker
    - init/iptables
    - init/haveged
    - init/nginx/all
    - init/sshd
    - init/sudo

    - iptables/save  # So we have the tables again when the system restarts. Arch always reloads them there.

  "shimmer.gserv.me":
    - init/nginx/shimmer

    - iptables/save  # So we have the tables again when the system restarts. Arch always reloads them there.
