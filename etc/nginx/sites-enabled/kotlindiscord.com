server {
    listen 80;

    server_name kotlindiscord.com;

    location / {
        return 301 https://wiki.kotlindiscord.com;
    }

    location /.well-known/acme-challenge {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;

    http2 on;

    server_name kotlindiscord.com;

    ssl_certificate /etc/letsencrypt/live/kotlindiscord.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/kotlindiscord.com/privkey.pem;

    location / {
        return 301 https://wiki.kotlindiscord.com;
    }

    location /.well-known/acme-challenge {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}
