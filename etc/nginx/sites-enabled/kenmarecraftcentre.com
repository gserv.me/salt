server {
    listen 80;
    listen 443 ssl;
    listen [::]:443 ssl;

    http2 on;

    server_name www.kenmarecraftcentre.com kenmarecraftcentre.com;

    ssl_certificate /etc/letsencrypt/live/kenmarecraftcentre.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/kenmarecraftcentre.com/privkey.pem;

    root /home/http/kenmarecraftcentre.com;

    location /.well-known/acme-challenge {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }

    location / {
        return 307 https://www.facebook.com/kenmarecrafts;
    }
}
