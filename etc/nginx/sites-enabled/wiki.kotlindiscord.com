server {
    listen 80;

    server_name wiki.kotlindiscord.com;

    location / {
        return 301 https://$host$request_uri;
    }

    location /.well-known/acme-challenge {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;

    http2 on;

    server_name wiki.kotlindiscord.com;

    ssl_certificate /etc/letsencrypt/live/wiki.kotlindiscord.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/wiki.kotlindiscord.com/privkey.pem;

    client_max_body_size 100M;

    location / {
        proxy_pass http://localhost:8106/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location /.well-known/acme-challenge {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}
