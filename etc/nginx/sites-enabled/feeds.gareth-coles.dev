server {
    listen 80;

    server_name feeds.gareth-coles.dev;

    location / {
        return 301 https://$host$request_uri;
    }

    location /.well-known/acme-challenge {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;

    http2 on;

    server_name feeds.gareth-coles.dev;

    ssl_certificate /etc/letsencrypt/live/feeds.gareth-coles.dev/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/feeds.gareth-coles.dev/privkey.pem;

    client_max_body_size 100M;

    location / {
        proxy_pass http://localhost:7050/;

		add_header X-Frame-Options SAMEORIGIN;
		add_header X-XSS-Protection "1; mode=block";

		proxy_buffering off;
		proxy_read_timeout 90;
		proxy_redirect off;

		proxy_pass_header Authorization;

		proxy_set_header Authorization $http_authorization;
        proxy_set_header Host $host;
        proxy_set_header Upgrade $http_upgrade;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
    }

    location /.well-known/acme-challenge {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}
