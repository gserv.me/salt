server {
    listen 80;
    listen 443 ssl;
    listen [::]:443 ssl;

    http2 on;

    server_name www.archivesmc.com archivesmc.com;

    ssl_certificate /etc/letsencrypt/live/archivesmc.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/archivesmc.com/privkey.pem;

    root /home/http/archivesmc.com;
    index index.html;

    location /.well-known/acme-challenge {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }

    location /renders {
        autoindex on;
        try_files $uri $uri/ =404;
    }

    location / {
        try_files $uri $uri/ =404;
    }
}
