from datetime import datetime
import logging
import re

import requests

from salt import exceptions

log = logging.getLogger(__name__)

BASE_URL = "https://discordapp.com/api/webhooks/{id}/{token}"
COLOURS = {
    "teal": 0x1abc9c,
    "dark_teal": 0x11806a,
    "green": 0x2ecc71,
    "dark_green": 0x1f8b4c,
    "blue": 0x3498db,
    "dark_blue": 0x206694,
    "purple": 0x9b59b6,
    "dark_purple": 0x71368a,
    "magenta": 0xe91e63,
    "dark_magenta": 0xad1457,
    "gold": 0xf1c40f,
    "dark_gold": 0xc27c0e,
    "orange": 0xe67e22,
    "dark_orange": 0xa84300,
    "red": 0xe74c3c,
    "dark_red": 0x992d22,
    "lighter_grey": 0x95a5a6,
    "light_grey": 0x979c9f,
    "darker_grey": 0x607d8b,
    "blurple": 0x7289da,
    "greyple": 0x99aab5
}

RET_TEMPLATE = {
    "name": "",
    "changes": {},
    "result": False,
    "comment": "",

}

URL_REGEX = re.compile(r"http[s]?://.*")


def _send_data(url, **data):
    for key, value in list(data.items()):
        # Remove any None values from the data; they're default values in functions

        if value is None:
            del data[key]

    return requests.post(url, json=data).text


def webhook(id, token, content, username=None, avatar=None, tts=False, name=None):
    if avatar and not URL_REGEX.match(avatar):
        raise exceptions.SaltInvocationError("Argument 'avatar' must be a valid HTTP/S URL.")

    name = name or "discord"

    ret = RET_TEMPLATE.copy()
    url = BASE_URL.format(id=id, token=token)

    ret["name"] = name

    try:
        resp = _send_data(url, content=content, username=username, avatar_url=avatar, tts=tts)
    except Exception as e:
        ret["comment"] += str(e)
    else:
        ret["comment"] += resp
        ret["result"] = True

    return ret


def webhook_embed(id, token, title=None, description=None, url=None,
                  timestamp=True, color=None, colour=None, thumbnail=None,
                  image=None, author=None, footer=None, fields=None,
                  username=None, avatar=None, name=None):
    if not any((title, description, footer, fields)):
        raise exceptions.SaltInvocationError("Must provide one of 'title', 'description', 'footer', 'fields'.")

    if avatar and not URL_REGEX.match(avatar):
        raise exceptions.SaltInvocationError("Argument 'avatar' must be a valid HTTP/S URL.")

    name = name or "discord"

    ret = RET_TEMPLATE.copy()
    hook_url = BASE_URL.format(id=id, token=token)

    ret["name"] = name

    embed = {}

    if title:
        embed["title"] = title

    if description:
        embed["description"] = description

    if url:
        if not URL_REGEX.match(url):
            raise exceptions.SaltInvocationError("Argument 'url' must be a valid HTTP/S URL.")

        embed["url"] = url

    if timestamp:
        now = datetime.now()
        embed["timestamp"] = now.isoformat()

    if color:
        colour = color

    if colour:
        if isinstance(colour, basestring):
            if colour in COLOURS:
                colour = COLOURS[colour]
            else:
                colours = sorted(list(COLOURS.keys()))

                raise exceptions.SaltInvocationError(
                    "Argument 'color'/'colour' must be a common colour name, or an integer. "
                    "Valid names: {colours}".format(
                        colours=", ".join(colours)
                    )
                )
        elif isinstance(colour, int):
            pass
        else:
            raise exceptions.SaltInvocationError("Argument 'colour' must be either a string or an integer.")

        embed["color"] = colour

    if thumbnail:
        if not URL_REGEX.match(thumbnail):
            raise exceptions.SaltInvocationError("Argument 'thumbnail' must be a valid HTTP/S URL.")

        embed["thumbnail"] = {"url": thumbnail}

    if image:
        if not URL_REGEX.match(image):
            raise exceptions.SaltInvocationError("Argument 'image' must be a valid HTTP/S URL.")

        embed["image"] = {"url": image}

    if author:
        author_name = author.get("name")
        author_url = author.get("url")
        author_icon = author.get("icon_url")

        if not author_name:
            raise exceptions.SaltInvocationError("Argument 'author' must have a 'name' key.")

        if author_url and not URL_REGEX.match(author_url):
            raise exceptions.SaltInvocationError(
                "Argument 'author' must either omit the 'url' key, or provide a valid HTTP/S URL.")

        if author_icon and not URL_REGEX.match(author_icon):
            raise exceptions.SaltInvocationError(
                "Argument 'author' must either omit the 'icon_url' key, or provide a valid HTTP/S URL.")

        author_object = {
            "name": author_name
        }

        if author_url:
            author_object["url"] = author_url

        if author_icon:
            author_object["icon_url"] = author_icon

        embed["author"] = author_object

    if footer:
        footer_text = footer.get("text")
        footer_icon = footer.get("icon_url")

        if not footer_text:
            raise exceptions.SaltInvocationError("Argument 'footer' must have a 'text' key.")

        if footer_icon and not URL_REGEX.match(footer_icon):
            raise exceptions.SaltInvocationError(
                "Argument 'footer' must either omit the 'icon_url' key, or provide a valid HTTP/S URL.")

        footer_object = {
            "text": footer_text
        }

        if footer_icon:
            footer_object["icon_url"] = footer_icon

        embed["footer"] = footer_object

    if fields:
        if not isinstance(fields, list):
            raise exceptions.SaltInvocationError("Argument 'fields' must be a list of dictionaries.")

        for i, field in enumerate(fields[:]):
            if "name" not in field:
                ret["comment"] += "Field {i} missing key: 'name'\n".format(i=i)
                fields.pop(i)
                continue

            if "value" not in field:
                ret["comment"] += "Field {i} missing key: 'value'\n".format(i=i)
                fields.pop(i)
                continue

        embed["fields"] = fields

    try:
        _send_data(hook_url, embeds=[embed], username=username, avatar_url=avatar)
    except Exception as e:
        ret["comment"] += str(e)
    else:
        ret["result"] = True

    return ret
