import docker
from docker.errors import NotFound


RET_TEMPLATE = {
    "name": "",
    "changes": {},
    "result": False,
    "comment": "",
}


def ensure(name, alias=None):
    client = docker.from_env()

    ret = RET_TEMPLATE.copy()
    ret["name"] = name

    try:
        plugin = client.plugins.get(alias or name)
        ret["result"] = True
        ret["comment"] += "Plugin already installed: {}".format(alias or name)

        if not plugin.enabled:
            plugin.enable()
            ret["comment"] += "\nPlugin enabled."
    except NotFound:  # We just don't have the plugin, so install it.
        plugin = client.plugins.install(name, local_name=alias)
        plugin.enable()

        ret["changes"] = {name: {"old": "", "new": name}}
        ret["comment"] += "Plugin installed: {}".format(alias or name)
        ret["result"] = True
    except Exception as e:
        ret["comment"] = str(e)
        ret["result"] = False

    return ret


def upgrade(name, target):
    client = docker.from_env()

    ret = RET_TEMPLATE.copy()
    ret["name"] = name

    try:
        client.plugins.get(target)

        ret["comment"] = "Plugin up to date: {}".format(target)
        ret["result"] = True

        return ret
    except NotFound:
        pass
    except Exception as e:
        ret["comment"] = str(e)
        ret["result"] = False

        return ret

    try:
        plugin = client.plugins.get(name)
    except Exception as e:
        ret["comment"] = str(e)
        ret["result"] = False
    else:
        do_enable = False

        try:
            if plugin.enabled:
                plugin.disable()
                do_enable = True

            plugin.upgrade(target)
            plugin.enable()
            do_enable = False

            ret["comment"] = "Plugin upgraded: {}".format(target)
            ret["changes"] = {name: {"old": name, "new": target}}
            ret["result"] = True
        except Exception as e:
            ret["comment"] = str(e)
            ret["result"] = False

        try:
            if do_enable:
                plugin.enable()
        except Exception as e:
            if ret["comment"]:
                ret["comment"] += "\n"

            ret["comment"] += "{}".format(e)

    return ret
